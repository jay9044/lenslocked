package controllers

import (
	"github.com/jay9044/lenslocked.com/views"
)

type Static struct {
	Homeview    *views.View
	ContactView *views.View
}

func NewStatic() *Static {
	return &Static{
		Homeview:    views.NewView("bootstrap", "static/homepage"),
		ContactView: views.NewView("bootstrap", "static/contact"),
	}
}
