package controllers

import (
	"net/http"

	"github.com/gorilla/schema"
)

//ParseForm - follows gurilla schema toolkit -- making it resuable
func ParseForm(dst interface{}, r *http.Request) error {
	//Decode takes an interface and map[string]string
	//dst is a interface can be any type of data
	err := r.ParseForm()
	if err != nil {
		return err
	}
	decoder := schema.NewDecoder()
	err = decoder.Decode(dst, r.PostForm)
	if err != nil {
		return err
	}

	//if there is no error
	return nil
}
