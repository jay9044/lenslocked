package controllers

import (
	"fmt"
	"net/http"

	"github.com/jay9044/lenslocked.com/views"
)

//Users -- to render a users view
//starts with new users return a user strcut and then rendering that user stcut
type Users struct {
	NewView *views.View
}

//Signup -- everythign we want from signup form
type Signup struct {
	//since schema is lowercase we dont need to change html case
	//struct is uppercase so without schema we would of had to change htnl name attribute to uppercase
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

//NewUsers - Views method form views package
func NewUsers() *Users {

	return &Users{
		NewView: views.NewView("bootstrap", "users/signup"),
	}
}

//New - GET /signup
func (u *Users) New(w http.ResponseWriter, r *http.Request) {
	// fmt.Println("is this working")
	err := u.NewView.Render(w, nil)
	if err != nil {
		panic(err)
	}
}

//Create - POST //what happens when user signsups
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {

	signUpForm := new(Signup)

	err := ParseForm(signUpForm, r)
	if err != nil {
		panic(err)
	}

	fmt.Fprintln(w, signUpForm)

}
