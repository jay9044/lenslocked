package views

import (
	"fmt"
	"net/http"
	"path/filepath"
	"text/template"
)

var (
	layoutDir   string = "views/layouts/"
	templateExt string = ".gohtml"
	templateDir string = "views/"
)

/*we are appending layout files to view files we pass to the funciton and then parsing
them as a whole. creating a new template/view */

// NewView exported functions must start with capital, like struct keys
func NewView(layout string, files ...string) *View {
	layoutFiles, err := filepath.Glob(layoutDir + "*" + templateExt)
	if err != nil {
		panic(err)
	}

	fmt.Println("before", files)
	addTemplateDirectory(files)
	fmt.Println("after Path", files)
	addTemplateExt(files)
	fmt.Println("after ext", files)

	for _, v := range layoutFiles {
		files = append(files, v)
	}
	t := template.Must(template.ParseFiles(files...))

	return &View{
		Template: t,
		Layout:   layout,
	}
}

//View - create views that parse footers automatically
type View struct {
	Template *template.Template
	Layout   string
}

//Render is used to render the view with the predefined layout
func (v *View) Render(w http.ResponseWriter, data interface{}) error {
	w.Header().Set("Content-Type", "text/html")
	return v.Template.ExecuteTemplate(w, v.Layout, data)
}

//so now handle methods in main.go will render automaticllay since the yuse the handler interface
//which goes off the servehttp function
func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := v.Render(w, nil)
	if err != nil {
		panic(err)
	}
}

//THIS IS NOT CURRENTLY USED BUT COULD BE
//we could remove out the for loop above and just append parselayout().. to files
func parselayout() []string {
	layouts, err := filepath.Glob(layoutDir + "*" + templateExt)
	if err != nil {
		panic(err)
	}

	return layouts
}

//e.g if file is users/contact
//it will add the temeplateDir varible which is views/
//so views/users/contact
func addTemplateDirectory(files []string) {
	for index, file := range files {
		files[index] = templateDir + file
	}
}

//e.g if file is users/contact
//it will add the temeplateExt varible which is .gohtml
//so,users/contact.gohtml
func addTemplateExt(files []string) {
	for index, file := range files {
		files[index] = file + templateExt
	}
}
