package main

import (
	"net/http"

	"github.com/jay9044/lenslocked.com/controllers"

	"github.com/gorilla/mux"
)

func main() {

	usersController := controllers.NewUsers()
	staticController := controllers.NewStatic()

	r := mux.NewRouter()

	r.Handle("/", staticController.Homeview).Methods("GET")
	r.Handle("/contact", staticController.ContactView).Methods("GET")

	r.HandleFunc("/signup", usersController.New).Methods("GET")
	r.HandleFunc("/signup", usersController.Create).Methods("POST")
	http.ListenAndServe(":8080", r)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
